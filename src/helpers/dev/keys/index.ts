/**
 *
 * @param e
 * @return {{keyCode: *, shiftKey: *, code: *, ctrlKey: *, altKey: *, key: *}}
 */
function createSubscriber(e) {
    const {keyCode, key, code, altKey, shiftKey, ctrlKey} = e
    return {keyCode, key, code, altKey, shiftKey, ctrlKey}
}

class ModalWindowsObserver {}

class KeysHandler {
    /**
     *
     * @type {[]}
     */
    subscribers = []

    watch() {
        window.addEventListener('keydown', e => {
            const subscriber = createSubscriber(e)
            this.add(subscriber)
        })
        window.addEventListener('keyup', e => this.remove(e.keyCode))
    }

    /**
     *
     * @param subscriber
     */
    add(subscriber) {
        this.subscribers.push(subscriber)
    }

    /**
     *
     * @param keyCode
     */
    remove(keyCode) {
        this.subscribers = this.subscribers.filter(subscriber => subscriber.keyCode !== keyCode)
    }

    /**
     *
     */
    run() {
        this.watch()
    }
}

/**
 *
 * @type {KeysHandler}
 */
const keysHandler = new KeysHandler()

export default keysHandler
