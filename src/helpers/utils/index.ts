export function convertServerResponse(res) {
    console.log(res)
    return {status: res.status, body: res.data || {}}
}

/**
 *
 * sortable array with different types by ABC
 *
 * @param arr
 * @param key
 */
export function sortByABC<T>(arr: Array<T>, key: string): Array<T> {
    arr = arr.sort((a: T, b: T) => {
        if (!a[key] || !b[key]) return -1
        if (a[key] < b[key]) return -1
        if (a[key] > b[key]) return 1
    })
    return arr
}

/**
 *
 * @param obj
 */
export function isString<T>(obj: T): boolean {
    return Object.prototype.toString.call(obj) === '[object String]'
}
