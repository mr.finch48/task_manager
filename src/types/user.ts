export enum Role {
    ADMIN = 'admin',
    EXECUTOR = 'executor',
    CONTROL = 'control',
    VIEWER = 'viewer'
}

export type User = {
    readonly id: number
    role?: UserRole
    fullName?: string
}

export type UserRole = Role.ADMIN | Role.EXECUTOR | Role.CONTROL | Role.VIEWER
