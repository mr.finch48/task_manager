import {Task} from "@/types/task"
import {Project} from "@/types/project"
import {UserRole} from "@/types/user"

export type Board = {
    readonly id: number
    title: string
    status: string
    owner: number
    tasks?: Task[]
    projects?: Project[]
    users?: Array<any>
    role: UserRole
}
