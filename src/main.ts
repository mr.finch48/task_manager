import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './sw'
import {Role} from "@/types/user"
import {EventSystem} from "@/config/event"
import focus from '@/directives/v-focus.ts'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/ru-RU'
Vue.use(ElementUI, {locale})

import Fragment from 'vue-fragment'
Vue.use(Fragment.Plugin)

import VueGates from 'vue-gates'
Vue.use(<any>VueGates)

export const EventBus = new Vue()

Vue.config.productionTip = false
Vue.prototype.Role = Role
Vue.prototype.EventBus = EventBus
Vue.prototype.EventSystem = EventSystem

Vue.directive('focus', focus)

new Vue({
    data: {
        Role
    },
    router,
    store,
    render: h => h(App)
}).$mount('#app')
