import {mapGetters} from "vuex";
import moment from "moment";
import {EventBus} from "@/main";
import {EventSystem} from "@/config/event.ts";

export default {
    name: 'CardTask',
    props: {
        task: {
            type: Object,
            required: true,
        }
    },
    data: () => ({
        loading: false,
    }),
    computed: {
        ...mapGetters({
            isTablet: ['viewport/isTablet'],
            users: ['board/getUsers'],
        }),
        clipTitle() {
            const {title} = this.task
            const maxLength = 140
            return title.length > maxLength ? `${title.substr(0, maxLength)}...` : title
        },
        term() {
            if (!this.task.term)
                return false
            const timestamp = new Date(this.task.term)
            return moment(timestamp.getTime()).format('до DD MMM') || null
        },
        isDeadline() {
            if (this.task.status === 'Просрочено') {
                // const timestamp = new Date(this.task.term)
                // if (Date.now() > timestamp) return true
                return true
            }
            return false
        },
    },
    created() {
        EventBus.$on('renderTaskPreloader', id => this.task.id === id ? this.loading = true : null)
        EventBus.$on('disableTaskPreloader', id => {
            this.task.id === id ? this.loading = false : null
        })
    }
}
