import Vue from "vue"
import Component from "vue-class-component"

interface ModalWindowController {
    open: () => void
    close: () => void
}

@Component
export class ModalWindow extends Vue implements ModalWindowController {

    //@ts-ignore
    private ctx = this.$options._componentTag

    open(): void {
        this.$root.$emit('render', {
            value: true,
            ctx: this.ctx
        })
    }

    close(): void {
        this.$root.$emit('render', {
            value: false,
            ctx: this.ctx
        })
    }

    beforeDestroy() {
        this.$root.$off('render')
    }
}
